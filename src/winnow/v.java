package winnow;

public class v {
	public static double beta = 1;
	public int m;
	public double[] value;
	public v(int m){
		this.m = m;
		this.value = new  double[m];
		for(int i=0; i<=m-1; i++){
			this.value[i]=1;
		}
		
	}
	
	public double mul(v another){
		double re= 0;
		if(this.m!= another.m){
			return 0;
		}
		for(int i = 0 ; i<=m-1; i++){
			re += Math.pow(1+beta,(this.value[i]-1)) *another.value[i];
		}
		return re;
	}
	
	public void enhance(v another){
		for(int i = 0 ; i<= m-1; i++){
			if(another.value[i]==1){
				this.value[i]+=1;
			}
			
		}
	}
	
	public void reduce(v another){
		for(int i = 0 ; i<= m-1; i++){
			if(another.value[i]==1){
				this.value[i]-=1;
			}
		}
	}
	
	public void print(){
		int relevant = 0;
		for(int i = 0 ; i<=this.m-1; i++){
			System.out.println(Math.pow(1+beta,(this.value[i]-1)));
			if(this.value[i]-1>=1){
				relevant+=1;
			}
		}
		System.out.println("Relevant feature is " + relevant);
	}
	
	public void printlog(){
		for(int i = 0 ; i<=this.m-1; i++){
			System.out.println(this.value[i]-1);
			System.out.print(" ");
		}
	}

}

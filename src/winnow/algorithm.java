package winnow;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class algorithm {
	
	
	public v winnowAlg(String inputfile){
		int m =50;
		v w = new v(m);
		
		try {
			FileInputStream inputStream = new FileInputStream(inputfile);
			Scanner sca = new Scanner(inputStream);
			while(sca.hasNext()){
				int bool = sca.nextInt();
				//System.out.println(bool);
				String notuse = sca.next();
				//System.out.println(notuse);
				v x = new v(m);
				for(int i = 0 ; i<= m -1; i ++){
					x.value[i] = sca.nextInt();
					//System.out.println(i + " "+x.value[i]);
				}
				
				int predict;
				if(w.mul(x)>=m){
					predict = 1;
				}
				else{
					predict = 0;
				}
				
				if((predict==1)&&(bool == 0)){
					
					w.reduce(x);
				}
				else if((predict==0)&&(bool == 1)){
					w.enhance(x);
				}
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Please put the HW2-A-...dat under same directory!");
		}
		
		return w;
	}
	
	public static float winnowTest(String inputfile, v result){
		int m = 50;
		float match = 0;
		float unmatch = 0;
		try {
			FileInputStream inputStream = new FileInputStream(inputfile);
			Scanner sca = new Scanner(inputStream);
			while(sca.hasNext()){
				int bool = sca.nextInt();
				String notuse = sca.next();
				v x = new v(m);
				for(int i = 0 ; i<= m -1; i ++){
					x.value[i] = sca.nextInt();
				}
				int predict;
				if(result.mul(x)>=m){
					predict = 1;
				}
				else{
					predict = 0 ;
				}
				
				
				if((predict == 1)&&(bool == 1)){
					//If an example is predicted to be 1 but the correct result was 0, all of the weights implicated in the mistake are set to 0 (demotion step).
					match+=1;
				}
				else if((predict == 0)&&(bool == 0)){
					//If an example is predicted to be 0 but the correct result was 1, all of the weights implicated in the mistake are multiplied by 2 (promotion step).
					match+=1;
				}
				else{
					unmatch+=1;
				}
				//System.out.println(predict+" "+ bool);
			}
		}catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Please put the HW2-A-...dat under same directory!");
		}
		
		return match/(match+unmatch);
	}
}
